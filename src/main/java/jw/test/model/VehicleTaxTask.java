package jw.test.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleTaxTask {
	private String label;
	private String model;
	private String plateNo;
	private String taskType;
	private String registrationNo = "Hello";
	private Double cost;
	private String dateValidTill;
	private String paidCheck;
	private Integer remindDay;
	private Boolean isUploadedPaymentReceipt;
	
	public VehicleTaxTask(String label, String model, String plateNo, String taskType, Double cost,
			String validTillDate, int isPaid, Integer remindDay, String regiNo, Integer receiptPhotoId) {
		this.label = label;
		this.model = model;
		this.plateNo = plateNo;
		this.taskType = taskTypeFullNaming(taskType);
		this.cost = cost;
		this.dateValidTill = validTillDate;
		this.registrationNo = regiNo;
		String dayLeftString = "";
		long dayLeft = calculateRemainingTime(validTillDate);
		if(receiptPhotoId != 0) {
			isUploadedPaymentReceipt = true;
		} else {
			isUploadedPaymentReceipt = false;
		}
		
		long defaultDayLeft = 50L;
		if(remindDay != null) defaultDayLeft = remindDay;
		
		if(defaultDayLeft > dayLeft) {
			dayLeftString = String.format("%s+ day(s) left.", dayLeft);
		} else {
			dayLeftString = String.format("%s+ day(s) left.", defaultDayLeft);
		} 

		this.paidCheck = isPaid == 1 ? "Paid" : String.format("Unpaid %s", dayLeftString);
	}

	public String statusCheck(String status) {
		String statusString = "";

		return statusString;
	}

	public String taskTypeFullNaming(String taskType) {
		switch (taskType) {
		case "K":
			taskType = "KIR";
			break;
		case "T":
			taskType = "Tax";
			break;
		case "C":
			taskType = "Certification";
			break;
		}

		return taskType;
	}

	
	public long calculateRemainingTime(String tillDate) {
		Date currentDate = new Date();
		int days = 0;
		try {
			Date dueDate = new SimpleDateFormat("yyyy-MM-dd").parse(tillDate);
			long diff = dueDate.getTime() - currentDate.getTime(); 
			return diff / (24 * 60 * 60 * 1000);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}		
	}
}