package jw.test.poi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jw.test.database.util.ResourceUtil;
import jw.test.model.VehicleTaxTask;

public class XlsGen {
	public static void main(String[] args) throws Exception {

		final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/fms_sigma?serverTimezone=UTC";

		final String USER_NAME = "fms";
		final String PASSWORD = "dnflskfk";

		Connection connection = null;
		Statement state = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM vhc_tax_task";
		List<VehicleTaxTask> vehicleTaxTaskList = new ArrayList<VehicleTaxTask>();

		try {
			Class.forName(JDBC_DRIVER);

			String url = DB_URL;

			connection = DriverManager.getConnection(url, USER_NAME, PASSWORD);
			state = connection.createStatement();
			rs = state.executeQuery(sql);
			System.out.println("연결 성공 ");

			while (rs.next()) {
				String label = rs.getString("label");
				String model = rs.getString("model");
				String plateNo = rs.getString("plate_no");
				String taskType = rs.getString("task_type");
				Double cost = rs.getDouble("cost");
				String validTillDate = rs.getString("valid_till_dt");
				int paidCheck = rs.getInt("paid");
				Integer remind = rs.getInt("remind");
				String regiNo = rs.getString("reg_no");
				Integer receiptPhotoId = rs.getInt("rcpt_photo_id");

				VehicleTaxTask vehicleTaxTask = new VehicleTaxTask(label, model, plateNo, taskType, cost, validTillDate,
						paidCheck, remind, regiNo, receiptPhotoId);
				vehicleTaxTaskList.add(vehicleTaxTask);
			}

			ByteArrayInputStream in = exportToExcel(vehicleTaxTaskList);
			createXlsFile(in);
			String filePrefix = "Tax_List";
			String.format("attachment; filename=%s_%s_%s.xlsx", filePrefix, "hello", "hello2");
		} catch (ClassNotFoundException e1) {
			// TODO: handle exception
			System.out.println("드라이버 로딩 실패 ");
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println("에러: " + e);
		} finally {
			ResourceUtil.closeAll(rs, state, connection);
		}

//		ByteArrayInputStream in = taxExportService.exportToExcel(vehicle);
//		System.out.println("In task/excel : ");
//		System.out.println(list.toString());
//
//		String filePrefix = "Tax_List";
//		String.format("attachment; filename=%s_%s_%s.xlsx", filePrefix, "hello", "hello");

//		return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/octet-stream")).headers(headers)
//				.body(new InputStreamResource(in));

	}

	private static String[] taxListColumnNames = { "Label", "Model", "Plate No.", "Type", "Payment No.", "Cost",
			"Due Date", "Status", "Payment Receipt" };

	private static String TITLE_OF_EXCEL_FILE = "Tax Report";
	
	public static ByteArrayInputStream exportToExcel(List<VehicleTaxTask> list) {
		Workbook xlsxWb = new XSSFWorkbook();
		final int firstColumnOffset = 1;
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		Sheet sheet1 = xlsxWb.createSheet("FirstSheet");
		
		//For Title
		Row titleRow = sheet1.createRow(0);
		Cell titleCell = titleRow.createCell(0);
		titleCell.setCellValue(TITLE_OF_EXCEL_FILE);
		CellStyle titleStyle = xlsxWb.createCellStyle();
		Font titleFont = xlsxWb.createFont();
		titleStyle.setAlignment(CellStyle.ALIGN_CENTER);
		titleFont.setFontHeightInPoints((short) 25);
		titleStyle.setFont(titleFont);
		titleCell.setCellStyle(titleStyle);
		sheet1.setColumnWidth(7, 256*100);
		sheet1.addMergedRegion(new CellRangeAddress(0,0,0,8));

		int index = -2;
		for (int i = firstColumnOffset; i < list.size() + firstColumnOffset + 1; i++) {
			
			Row row = sheet1.createRow(i);
			index++;
			for (int j = 0; j < taxListColumnNames.length; j++) {

				Cell cell = row.createCell(j);
				Font font = xlsxWb.createFont();
				CellStyle style = xlsxWb.createCellStyle();

				style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
				style.setAlignment(CellStyle.ALIGN_LEFT);
				style.setBorderLeft(CellStyle.BORDER_THIN);
				style.setBorderRight(CellStyle.BORDER_THIN);
				style.setBorderTop(CellStyle.BORDER_THIN);
				style.setBorderBottom(CellStyle.BORDER_THIN);
				if(j != 7) {
					sheet1.autoSizeColumn(j);	
				}
				

				if (i == firstColumnOffset) { // Column Name Setting
					style.setFillForegroundColor(IndexedColors.TEAL.getIndex());
					style.setFillPattern(CellStyle.SOLID_FOREGROUND);
					style.setAlignment(CellStyle.ALIGN_CENTER);
					font.setFontName("monospaced");
					font.setColor(IndexedColors.WHITE.getIndex());
					font.setFontHeightInPoints((short) 11);

					cell.setCellValue(taxListColumnNames[j]);
					style.setFont(font);

				} else {
					
					style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
					font.setColor(IndexedColors.BLACK.getIndex());
					switch (j) {
					case 0:
						cell.setCellValue(list.get(index).getLabel());
						break;
					case 1:
						cell.setCellValue(list.get(index).getModel());
						break;
					case 2:
						cell.setCellValue(list.get(index).getPlateNo());
						break;
					case 3:
						cell.setCellValue(list.get(index).getTaskType());
						break;
					case 4:
						cell.setCellValue(list.get(index).getRegistrationNo());
						break;
					case 5:
						cell.setCellValue(list.get(index).getCost().toString());
						break;
					case 6:
						cell.setCellValue(list.get(index).getDateValidTill());
						break;
					case 7:
						cell.setCellValue(list.get(index).getPaidCheck());
						break;
					case 8:
						cell.setCellValue(
								list.get(index).getIsUploadedPaymentReceipt() ? "Uploaded" : "Not Uploaded");
						break;
					}

				}
				cell.setCellStyle(style);
			}
		}

		try {
			xlsxWb.write(out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ByteArrayInputStream(out.toByteArray());

	}

	static SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH.mm.ss");

	public static void createXlsFile(InputStream in) throws Exception {
		OutputStream os = null;
		byte[] buffer = new byte[512];// 1kb buffer in memory
		int bytesRead;
		try {
			os = new FileOutputStream(new File(System.getProperty("user.home"),
					String.format("Desktop/%s_test.xlsx", formatter.format(new java.util.Date()))));
			while ((bytesRead = in.read(buffer)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
			System.out.println("created file... zzz");
		} finally {
			ResourceUtil.closeAll(os);
		}
	}


}
