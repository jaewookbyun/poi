package jw.test.database.util;

public class ResourceUtil {
	private ResourceUtil() {}
	public static void closeAll(AutoCloseable ... resources) {
		for(AutoCloseable resource : resources) {
			if(resource != null) try { resource.close(); } catch(Exception e) {}
		}
	}
}
